import React, {useState, useEffect} from 'react'


async function loadData(){
    const response = await fetch('http://localhost:8100/api/models/');
    if(response.ok){
        const modelData = await response.json();
        return modelData;
    } else {
        console.error('Failed to fetch model data');
        return null;
    }
}


function ModelList(){
    const[modelData, setModelData] = useState([]);

    useEffect(()=>{
        loadData().then(data => {
            setModelData(data.models);
        })
    }, []);

    return (
        <div className="container">
            <div className="my-5 card">
            <h3>Models</h3>
                <table className="table table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Name</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Manufacturer</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {modelData.map((model) => (
                            <tr key={model.id}>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{model.name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{model.manufacturer.name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}><img src={ model.picture_url } height="100" width="100" className="img-thumbnail"/></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default ModelList;
