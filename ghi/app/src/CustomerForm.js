import React, {useState} from 'react';

function CustomerForm(){
    const[firstName, setFirstName] = useState('');
    const[lastName, setLastName] = useState('');
    const[address, setAddress] = useState('');
    const[phoneNumber, setPhoneNumber] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const customer = {
            first_name: firstName,
            last_name: lastName,
            address: address,
            phone_number: phoneNumber
        };

        const customersURL = 'http://localhost:8090/api/customers/';
        const fetchConfig= {
            method: 'post',
            body: JSON.stringify(customer),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(customersURL, fetchConfig);
        if(response.ok){
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
        }
    }

    return (
        <div className="container">
            <div className="my-5">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-customer-form">
                                    <h1 className="card-title text-center">New Customer</h1>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleFirstNameChange} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control" value={firstName}/>
                                            <label htmlFor="first_name">First Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleLastNameChange} required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control" value={lastName}/>
                                            <label htmlFor="last_name">Last Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleAddressChange} required placeholder="address" type="text" id="address" name="address" className="form-control" value={address}/>
                                                <label htmlFor="address">Address</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <input onChange={handlePhoneNumberChange} required placeholder="phone_number" type="text" id="phone_number" name="phone_number" className="form-control" value={phoneNumber}/>
                                            <label htmlFor="phone_number">Phone Number</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-outline-primary btn-lg btn-block">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm
