import React, {useState, useEffect} from 'react'


function AutomobileForm(){
    const[color, setColor] = useState('');
    const[year, setYear] = useState('');
    const[vin, setVin] = useState('');
    const[model, setModel] = useState('');
    const[models, setModels] = useState([]);

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const fetchData = async() => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json()  ;
            setModels(data.models);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok){
            setColor('');
            setYear('');
            setVin('');
            setModel('');
        }
    }

    useEffect(() =>{
        fetchData();
    }, []);

    return (
        <div className="container ">
            <div className="my-5">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-appt-form">
                                    <h3 className="card-title text-center">New Automobile</h3>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleColorChange} required placeholder="Color" type="text" id="color" name="color" className="form-control" value={color}/>
                                                <label htmlFor="color">Color</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleYearChange} required placeholder="Year" type="text" id="year" name="year" minLength='4' maxLength="4" className="form-control" value={year}
                                                onKeyDown={ (event) => {
                                                    if (!/[0-9]/.test(event.key) && event.key !== 'Backspace' && event.key !== 'Delete') {
                                                        event.preventDefault();
                                                    }
                                                }}/>
                                                <label htmlFor="year">Year</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleVinChange} required placeholder="VIN" type="text" id="vin" name="vin" minLength='17' maxLength="17" className="form-control" value={vin}/>
                                                <label htmlFor="vin">VIN</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className=" row mb-3">
                                        <div className="col">
                                            <select onChange={handleModelChange} name="model" id="model" required value={model} className="form-select mb-3">
                                                <option value="">Choose a Model</option>
                                                {models.map((model) =>(
                                                    <option key={model.id} value={model.id}> {model.name} </option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-outline-primary btn-lg btn-block">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default AutomobileForm;
