import React, {useState, useEffect} from 'react';

function SalespersonForm(){
    const[firstName, setFirstName] = useState('');
    const[lastName, setLastName] = useState('');
    const[employeeId, setEmployeeId] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const salesperson = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId,
        };

        const salespeopleURL = 'http://localhost:8090/api/salespeople/';
        const fetchConfig= {
            method: 'post',
            body: JSON.stringify(salesperson),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salespeopleURL, fetchConfig);
        if(response.ok){
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    }

    return (
        <div className="container">
            <div className="my-5">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-salesperson-form">
                                    <h1 className="card-title text-center">New Salesperson</h1>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleFirstNameChange} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control" value={firstName}/>
                                            <label htmlFor="first_name">First Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleLastNameChange} required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control" value={lastName}/>
                                            <label htmlFor="last_name">Last Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleEmployeeIdChange} required placeholder="employee_id" type="text" id="employee_id" name="employee_id" className="form-control" value={employeeId}/>
                                                <label htmlFor="employee_id">Employee ID</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-outline-primary btn-lg btn-block">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SalespersonForm
