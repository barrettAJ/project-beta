import React, { useState, useEffect } from 'react';

export default function SaleList() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([])
    const [alertVisible, setAlertVisible] = useState(
        localStorage.getItem('alertVisible') === 'true'
    );
    const [selectedSalesperson, setSelectedSalesperson] = useState('')

    useEffect(() => {
        fetchSales();
        fetchSalespeople();
    }, []);

    useEffect(() => {
        if (alertVisible) {
            setTimeout(() => {
                setAlertVisible(false);
                localStorage.setItem('alertVisible', 'false');
            }, 2000);
        }
    }, [alertVisible]);

    const fetchSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        } else {
        console.error('Error fetching sales:', response.status);
        }
    };

    const fetchSalespeople = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        } else {
        console.error('Error fetching salespeople:', response.status);
        }
    };

    const handleChangeSalesperson = (event) => {
        const value = event.target.value;
        setSelectedSalesperson(value);
    };

    const filteredSales = selectedSalesperson
    ? sales.filter((sale) => sale.salesperson.employee_id == selectedSalesperson)
    : sales;

return (
<div className="container">
    <div className="my-5 card">
        <h3>Sales History</h3>
            <select value={selectedSalesperson} onChange={handleChangeSalesperson}>
            <option value="">All salespeople</option>
            {salespeople.map((salesperson) => (
                <option key={salesperson.id} value={salesperson.employee_id}>
                {`${salesperson.first_name} ${salesperson.last_name}`}
                </option>
            ))}
            </select>
    <table className="table table-striped">
        <thead className="bg-dark text-light">
        <tr>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Salesperson</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Customer</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIN</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Price</th>
        </tr>
        </thead>
        <tbody>
        {filteredSales.length === 0 ? (
            <tr>
            <td colSpan="5" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                No sale data available
            </td>
            </tr>
        ) : (
            filteredSales.map((sale, index) => (
            <tr key={sale.id}>
                <th scope="row" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {index + 1}
                </th>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {`${sale.salesperson.first_name} ${sale.salesperson.last_name} (${sale.salesperson.employee_id})`}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {`${sale.customer.first_name} ${sale.customer.last_name}`}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {sale.automobile.vin}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {sale.price}
                </td>
            </tr>
            ))
        )}
        </tbody>
    </table>
    </div>
</div>
);
}
