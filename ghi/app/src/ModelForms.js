import React, {useState, useEffect} from 'react'

function ModelForm(){
    const[modelName, setModelName] = useState('');
    const[picture, setPicture] = useState('');
    const[manufacturer, setManufacturer] = useState('');
    const[manufacturers, setManufacturers] = useState([]);

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const fetchData = async() =>{
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = modelName;
        data.picture_url = picture;
        data.manufacturer_id = manufacturer;
        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(modelUrl, fetchConfig);
        if(response.ok){
            setModelName('');
            setPicture('');
            setManufacturer('');
        }
    }

    useEffect(() =>{
        fetchData();
    }, []);

    return (
        <div className="container ">
            <div className="my-5">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-appt-form">
                                    <h3 className="card-title text-center">New Model</h3>
                                    <div className=" row mb-3">
                                        <div className="col">
                                            <select onChange={handleManufacturerChange} name="manufacturer" id="manufacturer" required value={manufacturer} className="form-select mb-3">
                                                <option value="">Choose a Manufacturer</option>
                                                {manufacturers.map((manufacturer) =>(
                                                    <option key={manufacturer.id} value={manufacturer.id}> {manufacturer.name} </option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleModelNameChange} required placeholder="Model" type="text" id="model" name="model" className="form-control"value={modelName}/>
                                                <label htmlFor="model">Model</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handlePictureChange} required placeholder="Image URL" type="url" id="picture" name="picture" className="form-control" value={picture}/>
                                                <label htmlFor="picture">Image</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-outline-primary btn-lg btn-block">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default ModelForm;
