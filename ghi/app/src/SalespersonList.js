import React, { useState, useEffect } from 'react';

export default function SalespersonList() {
    const [salespeople, setSalespeople] = useState([]);

    useEffect(() => {
        fetchSalespeople();
    }, []);

    const fetchSalespeople = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        } else {
        console.error('Error fetching salespeople:', response.status);
        }
    };

return (
<div className="container">
    <div className="my-5 card">
    <h3>Salespeople</h3>
    <table className="table table-striped">
        <thead className="bg-dark text-light">
        <tr>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col"></th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">First Name</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Last Name</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Employee ID</th>
        </tr>
        </thead>
        <tbody>
        {salespeople.length === 0 ? (
            <tr>
            <td colSpan="4" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                No salesperson data available
            </td>
            </tr>
        ) : (
            salespeople.map((salesperson, index) => (
            <tr key={salesperson.employee_id}>
                <th scope="row" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {index + 1}
                </th>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {salesperson.first_name}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {salesperson.last_name}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {salesperson.employee_id}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                </td>
            </tr>
            ))
        )}
        </tbody>
    </table>
    </div>
</div>
);
}
