import React, {useState, useEffect} from 'react'


async function loadData(){
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if(response.ok){
        const modelData = await response.json();
        return modelData;
    } else {
        console.error('Failed to fetch automobile data');
        return null;
    }
}


function AutomobileList(){
    const[autoData, setAutoData] = useState([]);

    useEffect(()=>{
        loadData().then(data => {
            setAutoData(data.autos);
        })
    }, []);

    return (
        <div className="container">
            <div className="my-5 card">
            <h3>Automobiles</h3>
                <table className="table table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIN</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Color</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Year</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Model</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Manufacturer</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Sold</th>
                        </tr>
                    </thead>
                    <tbody>
                        {autoData.map((auto) => (
                            <tr key={auto.id}>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.vin}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.color}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.year}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.model.name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.model.manufacturer.name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{auto.sold ? 'Yes' : 'No'}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default AutomobileList;
