import React, { useState, useEffect } from 'react';

export default function SaleList() {
    const [sales, setSales] = useState([]);

    useEffect(() => {
        fetchSales();
    }, []);

    const fetchSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        } else {
        console.error('Error fetching sales:', response.status);
        }
    };

return (
<div className="container">
    <div className="my-5 card">
    <h3>Sales</h3>
    <table className="table table-striped">
        <thead className="bg-dark text-light">
        <tr>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col"></th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIN</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Salesperson</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Customer</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Price</th>
        </tr>
        </thead>
        <tbody>
        {sales.length === 0 ? (
            <tr>
            <td colSpan="10" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                No sale data available
            </td>
            </tr>
        ) : (
            sales.map((sale, index) => (
            <tr key={sale.id}>
                <th scope="row" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {index + 1}
                </th>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {sale.automobile.vin}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {`${sale.salesperson.first_name} ${sale.salesperson.last_name} (${sale.salesperson.employee_id})`}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {`${sale.customer.first_name} ${sale.customer.last_name}`}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {'$' + sale.price}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                </td>
            </tr>
            ))
        )}
        </tbody>
    </table>
    </div>
</div>
);
}
