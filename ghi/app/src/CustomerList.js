import React, { useState, useEffect } from 'react';

export default function CustomerList() {
    const [customers, setCustomers] = useState([]);

    useEffect(() => {
        fetchCustomers();
    }, []);

    const fetchCustomers = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        } else {
        console.error('Error fetching customers:', response.status);
        }
    };

return (
<div className="container">
    <div className="my-5 card">
    <h3>Customers</h3>
    <table className="table table-striped">
        <thead className="bg-dark text-light">
        <tr>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col"></th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">First Name</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Last Name</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Address</th>
            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Phone Number</th>
        </tr>
        </thead>
        <tbody>
        {customers.length === 0 ? (
            <tr>
            <td colSpan="10" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                No customer data available
            </td>
            </tr>
        ) : (
            customers.map((customer, index) => (
            <tr key={customer.id}>
                <th scope="row" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {index + 1}
                </th>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {customer.first_name}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {customer.last_name}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {customer.address}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                {customer.phone_number}
                </td>
                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                </td>
            </tr>
            ))
        )}
        </tbody>
    </table>
    </div>
</div>
);
}
