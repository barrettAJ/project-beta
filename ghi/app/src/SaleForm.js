import React, {useState, useEffect} from 'react';

function SaleForm(){
    const[automobiles, setAutomobiles] = useState([]);
    const[salespeople, setSalespeople] = useState([]);
    const[customers, setCustomers] = useState([]);
    const [selectedAutomobile, setSelectedAutomobile] = useState('');
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [selectedCustomer, setSelectedCustomer] = useState('');
    const[price, setPrice] = useState('');


    useEffect(() => {
        const fetchSalesAndAutomobiles = async () => {
            try {
                const salesResponse = await fetch('http://localhost:8090/api/sales/');
                const salesData = await salesResponse.json();
                const soldAutomobiles = salesData.sales.map((sale) => sale.automobile.vin);
                const inventoryResponse = await fetch('http://localhost:8100/api/automobiles/');
                const inventoryData = await inventoryResponse.json();
                const unsoldAutomobiles = inventoryData.autos.filter((automobile) => !soldAutomobiles.includes(automobile.vin));
                setAutomobiles(unsoldAutomobiles);
            } catch (error) {
                console.error('Error fetching sales and automobiles:', error);
            }
        };

        fetchSalesAndAutomobiles();


        fetch('http://localhost:8090/api/salespeople/')
            .then((response) => response.json())
            .then((data) => {
                setSalespeople(data.salespeople);
            })
            .catch((error) => {
                console.error('Error fetching salespeople:', error);
            });


        fetch('http://localhost:8090/api/customers/')
            .then((response) => response.json())
            .then((data) => {
                setCustomers(data.customers);
            })
            .catch((error) => {
                console.error('Error fetching customers:', error);
            });
    }, []);

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setSelectedAutomobile(value);
    }
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSelectedSalesperson(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setSelectedCustomer(value);
    }
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const sale = {
            automobile: selectedAutomobile,
            salesperson: selectedSalesperson,
            customer: selectedCustomer,
            price: price
        };

        const salesURL = 'http://localhost:8090/api/sales/';
        const fetchConfig= {
            method: 'post',
            body: JSON.stringify(sale),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesURL, fetchConfig);
        const autoURL = `http://localhost:8100/api/automobiles/${selectedAutomobile}/`;
        const autoSale = {
            sold: true
        };
        const fConfig = {
            method: 'PUT',
            body: JSON.stringify(autoSale),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const autoResponse = await fetch(autoURL, fConfig);

        if(response.ok){
            const newSale = await response.json();
            setSelectedAutomobile('');
            setSelectedSalesperson('');
            setSelectedCustomer('');
            setPrice('');

            setAutomobiles((prevAutomobiles) => {
                return prevAutomobiles.filter((automobile) => automobile.vin !== selectedAutomobile);
            });
        }
    };

    return (
        <div className="container">
            <div className="my-5">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-sale-form">
                                    <h1 className="card-title text-center">New Sale</h1>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <select onChange={handleAutomobileChange} required placeholder="automobile" type="text" id="automobile" name="automobile" className="form-control" value={selectedAutomobile}>
                                            <option value="">Select an automobile</option>
                                            {automobiles.map((automobile) => (
                                                <option key={automobile.id} value={automobile.vin}>
                                                {automobile.vin}
                                                </option>
                                            ))}
                                            </select>
                                            <label htmlFor="automobile">Automobile VIN</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <select onChange={handleSalespersonChange} required placeholder="salesperson" type="text" id="salesperson" name="salesperson" className="form-control" value={selectedSalesperson}>
                                            <option value="">Select a salesperson</option>
                                            {salespeople.map((salesperson) => (
                                                <option key={salesperson.id} value={salesperson.employee_id}>
                                                    {salesperson.first_name} {salesperson.last_name}
                                                </option>
                                            ))}
                                            </select>
                                            <label htmlFor="salesperson">Salesperson</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <select onChange={handleCustomerChange} required placeholder="customer" type="text" id="customer" name="customer" className="form-control" value={selectedCustomer}>
                                            <option value="">Select a customer</option>
                                            {customers.map((customer) => (
                                                <option key={customer.id} value={customer.id}>
                                                    {customer.first_name} {customer.last_name}
                                                </option>
                                            ))}
                                            </select>
                                                <label htmlFor="customer">Customer</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <input onChange={handlePriceChange} required placeholder="price" type="text" id="price" name="price" className="form-control" value={price}/>
                                            <label htmlFor="price">Price</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-outline-primary btn-lg btn-block">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SaleForm
