from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)

from .models import Salesperson, Customer, Sale, AutomobileVO


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse({"salespeople": salespeople}, encoder=SalespersonEncoder)
    else:
        try:
            content = json.loads(request.body)
            _ = content["first_name"]
            _ = content["last_name"]
            _ = content["employee_id"]
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                {
                    "salesperson": salesperson,
                    "message": "Salesperson added successfully",
                },
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the salesperson"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_salesperson(request, employee_id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            return JsonResponse(
                {"salesperson": salesperson}, encoder=SalespersonEncoder, safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            salesperson.delete()
            return JsonResponse(
                {
                    "message": f"Salesperson with employee ID {employee_id} deleted successfully"
                }
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            props = ["first_name", "last_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
            salesperson.save()
            return JsonResponse(
                {
                    "salesperson": salesperson,
                    "message": "Salesperson updated successfully",
                },
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    else:
        try:
            content = json.loads(request.body)
            _ = content["first_name"]
            _ = content["last_name"]
            _ = content["address"]
            _ = content["phone_number"]
            customer = Customer.objects.create(**content)
            return JsonResponse(
                {"customer": customer, "message": "Customer added successfully"},
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the customer"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                {"customer": customer}, encoder=CustomerEncoder, safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                {
                    "message": f"Customer {customer.first_name} {customer.last_name} deleted successfully"
                }
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)
            props = ["first_name", "last_name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                {"customer": customer, "message": "Customer updated successfully"},
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder=SaleEncoder)
    else:
        try:
            content = json.loads(request.body)
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            employee_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            price = content["price"]
            sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=price,
            )
            automobile.sold = True
            automobile.save()

            return JsonResponse(
                {"sale": sale, "message": "Sale added successfully"},
                encoder=SaleEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Automobile with the provided VIN does not exist"}
            )
            response.status_code = 404
            return response
        except (Salesperson.DoesNotExist, Customer.DoesNotExist):
            response = JsonResponse(
                {"message": "Salesperson or Customer does not exist"}
            )
            response.status_code = 404
            return response
        except:
            response = JsonResponse({"message": "Could not create the sale"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse({"sale": sale}, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse({"message": "Sale deleted successfully"})
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.get(id=pk)
            props = ["automobile", "salesperson", "customer", "price"]
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                {"sale": sale, "message": "Sale updated successfully"},
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"})
            response.status_code = 404
            return response
