# CarCar

Team:

* Austin B. - Services
* Corissa W. - Sales

## Design
![Alt text](design.png)
## Service microservice

The Service microservice contains the following models:
-Technician - Contains first and last name along with an employee ID(unique integer).
-Appointment - Handles all of the appointment logic along with methods to cancel/finish an appointment.
-AutomobileVO - Contains the data(import_href, vin and sold) polled from inventory service.

## Sales microservice

The Sales microservice handles sales operations through the management of salespeople, customers, and sales records. This microservice includes the following models:

Salesperson: This model represents salespeople and includes fields for their first name, last name, and employee ID. Each salesperson is uniquely identified by their employee ID.

Customer: This model contains information about customers and includes fields for their first name, last name, address, and phone number. It allows for the identification and contact details of customers.

Sale: This model manages sales records and includes fields for the automobile, salesperson, customer, and price.

AutomobileVO: This model stores information about automobiles obtained from the inventory service through polling. It includes fields such as the VIN (Vehicle Identification Number) and sold status. The integration with the Inventory microservice also ensures that only available and unsold automobiles can be sold.
